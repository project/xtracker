
Copyright 2005 http://2bits.com

Module
------
xtracker: Extended Tracker

Description
-----------
This module provides a drop in replacement for the core tracker module,
but with the following differences:

- Ability to display terms from a certain category as tabs, then users
  can restrict the listing to the one term in the category

- Ability to display content types as tabs, then users can restrict the
  listing to the one content type (blog, image, page, ...etc.)

- Interfaces with the nodevote.module and displays score for each node

- All columns displayed are sortable, so the output can be sorted by 
  node type, author name, title, number of replies, date of last reply,
  or the score from node vote.

- Configurable number of nodes per page

Note: If the selected category has too many terms, some themes will
display all the tabs on a single line, making the page too wide.

Bluemarine, marvin, and chamelon all do that. Civicspace theme 
wraps the tabs around, and has no such problem.

Under the hood, the queries have been simplified and parameterized,
so as to make the module more maintainable.

Installation
------------
To install this module, do the following:

Upload or copy the xtracker.module file to your modules directory.

Enable it from administer -> modules.

Visit administer -> settings -> xtracker and set the options you want.

Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.

Author
------
Khalid Baheyeldin (http://baheyeldin.com/khalid and http://2bits.com)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.

The author can also be contacted for paid customizations of this
and other modules.
